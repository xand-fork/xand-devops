![t4 logo](../img/t4.png) ![eks logo](../img/eks.png) ![k8s logo](../img/k8s.png) ![tpfs logo](../img/tpfs.png)

# Terraform EKS

This will set up an auto repairing, upgrading, scaling [EKS (Amazon Elastic Kubernetes Service)](https://aws.amazon.com/eks) cluster using [Terraform](https://www.terraform.io/) with [S3 / DynamoDB](https://www.terraform.io/docs/backends/types/s3.html) as the [Terraform Remote Storage](https://www.terraform.io/docs/state/remote.html) backend.

## Background

The [Xand Platform Runbook](https://transparentinc.atlassian.net/wiki/spaces/XNS/pages/259915781/Xand+Platform+Runbook) has background context on why this exists and how it fits into our infrastructure.

## Pre-requisites

- Artifactory username and password
- [Install Terraform](https://www.terraform.io/downloads.html)
- [Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Install AWS CLI version 2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html)
- Clone this repo locally
- User deploying cluster must have AdministratorAccess permissions. This user must have “programmatic access” with associated [access key ID and secret access key](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey)

## Set the service account credentials

To obtain the service account credentials talk to someone in the [DevOps Guild](https://transparentinc.atlassian.net/wiki/spaces/TT/pages/279969855/DevOps+Guild).

Then set the following environment variable correctly so that you can authenticate with AWS.

https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html

## Set up Terraform Backend

To create the Terraform backend and update terraform to use the configured bucket as a backend, Set the following environment variables and run the script as shown:

EKS is only supported in these 3 regions in the US: [ "us-east-1", "us-east-2", "us-west-2" ]
Source: https://aws.amazon.com/about-aws/whats-new/2020/04/eks-adds-fargate-support-in-frankfurt-oregon-singapore-and-sydney-aws-regions/

```
export STORAGE_CONTAINER=terraform-xand
export REGION=us-west-2

./scripts/create-aws-storage.sh "$STORAGE_CONTAINER" "$REGION"
```
In addition to checking if the bucket exists and creating it if it doesn't, the script configures main.tf with the specified bucket name and region.

## Workspace Environments

Terraform uses the concept of a [workspace](https://www.terraform.io/docs/state/workspaces.html) for managing state. We use a workspace to define multiple environments whether shared or personal. For example shared workspace environments could contain `dev`, `test`, `qa` or `prod` stages for a shared project such as `xand-dev` or `xand-prod`. 

With the Terraform backend configured, run `./scripts/init_tf_env.sh {MY_WORKSPACE_NAME}` script to

- initialize any plugins required
- connect to the configured s3 storage
- initalize the Terraform workspace if it doesn't exist

For example:
```bash
./scripts/init_tf_env.sh "<USERNAME>-prod"
```

initializes a workspace.

See existing workspaces tracked in the configured backend with:
```bash
terraform workspace list
```

## Set up Terraform variable files (`.tfvars`)

Personal environments allow you to name your own cluster for each stage and specify resources.

An AWS SSH EC2 key-pair must be created prior to setting up the cluster for SSH access to cluster nodes. To create this using the AWS CLI, run the following command:

```
aws ec2 create-key-pair --key-name xand-ssh --region $REGION > xand_ssh_creds.txt
```

You should store the private ssh key contained in xand_ssh_creds.txt somewhere secure for later use in managing the cluster.

Terraform requires a configuration file to set up the cluster. See an example terraform configuration at example.tfvars. To make your own,
copy it to a new file and then update its variables. TPFS should provide you the Artifactory details.

```
cp "example.tfvars" "production.tfvars"
$EDITOR "production.tfvars"
```

## Deploy a new cluster

Now that Terraform has been properly configured go ahead and tell it to apply the configuration and launch the cluster.

```
terraform apply -var-file="<USERNAME>-prod.tfvars"
```

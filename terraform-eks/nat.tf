### NAT Gateway
### https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html
### https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html#Add_IGW_Routing

resource "aws_eip" "xand_nat" {
  vpc              = true
  public_ipv4_pool = "amazon"
}

resource "aws_subnet" "xand_nat" {
  vpc_id = aws_vpc.xand.id

  cidr_block     = "192.168.254.0/24"

  depends_on     = [aws_internet_gateway.xand]

  tags = map(
    "Name", "${var.cluster_name}-nat",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "propagate_at_launch", true
  )
}

resource "aws_route_table" "xand_nat" {
  vpc_id = aws_vpc.xand.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.xand.id
  }

  tags = map(
    "Name", "${var.cluster_name}-nat",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "propagate_at_launch", true
  )
}

resource "aws_route_table_association" "xand_nat" {
  count = var.subnet_count

  route_table_id = aws_route_table.xand_nat.id
  subnet_id      = aws_subnet.xand_nat.id
}

resource "aws_nat_gateway" "xand" {
  depends_on     = [aws_subnet.xand_nat]

  allocation_id  = aws_eip.xand_nat.id
  subnet_id      = aws_subnet.xand_nat.id

  tags = map(
    "Name", "${var.cluster_name}",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "propagate_at_launch", true
  )
}

resource "aws_route_table" "xand_nodes" {
  vpc_id = aws_vpc.xand.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.xand.id
  }

  tags = map(
    "Name", "${var.cluster_name}-nodes",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "propagate_at_launch", true
  )
}

resource "aws_route_table_association" "xand_nodes" {
  count = var.subnet_count

  route_table_id = aws_route_table.xand_nodes.id
  subnet_id      = aws_subnet.xand_nodes.*.id[count.index]
}

variable "cluster_name" {
  type        = string
  description = "The K8S cluster name"
}

variable "cluster_version" {
  type        = string
  default     = "1.22"
  description = "The K8S cluster version"
}

variable "aws_region" {
  type        = string
  default     = "us-west-2"
  description = "AWS region"
}

variable "namespace_name" {
  default = "xand-service"
  type    = string
}

variable "subnet_count" {
  default = 2
}

variable "aws_instance_type" {
  default = "m5.2xlarge"
  type = string
}

variable "aws_scaling_min_size" {
  default = 2
}

variable "aws_scaling_max_size" {
  default = 6
}

variable "aws_ssh_keypair_name" {
  type        = string
}

variable "container_runtime" {
  type        = string
  description = "Container runtime used by EKS worker nodes. Allowed values: `dockerd` and `containerd`."
  default     = "containerd"
}

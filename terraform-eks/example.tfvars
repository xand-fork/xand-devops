# Name is re-used for corresponding node-pool.
# Must <= 12 characters and only contain [a-z0-9]
cluster_name = "{CLUSTER_NAME}"

# AWS EC2 key pair: This must be created in AWS first.
aws_ssh_keypair_name = "xand-ssh"

# AWS Settings
aws_region  = "us-west-2"
aws_instance_type = "m5.2xlarge"
aws_scaling_min_size = 2
aws_scaling_max_size = 4

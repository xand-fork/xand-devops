resource "local_file" "aws_auth" {
  depends_on = [
    aws_eks_cluster.xand,
    aws_autoscaling_group.xand
  ]

  content = templatefile("${path.module}/aws-auth.tpl", {
    cluster_node_arn = aws_iam_role.cluster_node.arn,
    cluster_arn = aws_iam_role.cluster.arn
  })
  filename = "${path.module}/aws-auth.yaml"
}

resource "null_resource" "update_kubeconfig" {
  depends_on = [
    local_file.aws_auth
  ]

  # Always run in case the .kube/config file doesn't exist locally.
  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "aws eks --region ${var.aws_region} update-kubeconfig --name ${aws_eks_cluster.xand.name}"
  }

  provisioner "local-exec" {
    command = "kubectl apply -f aws-auth.yaml && rm aws-auth.yaml"
  }
}

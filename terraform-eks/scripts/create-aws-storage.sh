#!/usr/bin/env bash

set -o errexit # abort on nonzero exit status
set -o nounset # abort on unbound variable
set -o pipefail # abort if any element of a pipeline fails

function helptext () {
    local text=$(cat <<EOF
    Set up AWS S3 bucket for Terraform workspace

    Arguments
        STORAGE_CONTAINER (Required) = AWS S3 Bucket name to be created.
        REGION (Optional) = AWS region for the blob storage. Defaults to 'us-west-2'.
EOF
	  )
    echo "$text"
}

function error () {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

if [[ $# -eq 0 ]]; then
    error "Not enough arguments provided"
fi

STORAGE_CONTAINER=${1:?"$(error 'STORAGE_CONTAINER must be set')"}
REGION=${2:-"us-west-2"}

if aws sts get-caller-identity > /dev/null 2>&1 ; then
    info "Already logged into AWS"
else
    info "Logging into AWS ..."
    aws configure
fi

if aws s3api head-bucket --bucket "$STORAGE_CONTAINER" > /dev/null 2>&1 ; then
    info "S3 bucket '${STORAGE_CONTAINER}' found, and logged in IAM user has permissions to access it."

    #   if we found the bucket, we should update the region so it is set appropriately in main.tf later
    
    REGION="$(aws s3api get-bucket-location --bucket $STORAGE_CONTAINER --output text)"
    info "Updated region to ${REGION} ..."

else
    info "Creating an AWS S3 bucket called '${STORAGE_CONTAINER}' in the ${REGION} region ..."
    aws s3api create-bucket \
        --bucket "$STORAGE_CONTAINER" \
        --region $REGION \
        --create-bucket-configuration LocationConstraint=$REGION
fi

if aws s3api get-bucket-encryption --bucket "$STORAGE_CONTAINER" > /dev/null 2>&1 ; then
    info "Default bucket encryption already set."
else
    info "Setting default encryption to AES256 ..."
    aws s3api put-bucket-encryption \
        --bucket "$STORAGE_CONTAINER" \
        --server-side-encryption-configuration '{"Rules": [{"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}}]}'
fi

if aws s3api get-public-access-block --bucket "$STORAGE_CONTAINER" > /dev/null 2>&1 ; then
    info "Bucket is already has public access blocked."
else 
    info "Blocking public access for '${STORAGE_CONTAINER}' ..."
    aws s3api put-public-access-block \
        --bucket "$STORAGE_CONTAINER" \
        --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
fi

if aws s3api get-bucket-versioning --bucket "$STORAGE_CONTAINER" > /dev/null 2>&1 ; then
    info "Bucket already has versioning turned on."
else 
    info "Blocking public access for '${STORAGE_CONTAINER}' ..."
    aws s3api put-bucket-versioning \
        --bucket "$STORAGE_CONTAINER"
        --versioning-configuration Status=Enabled
fi

info "Successfully provisioned and configured an S3 bucket for Terraform!"

#   Create DYNAMODB_TABLE - note that multiple buckets can use the same table, so shared name is fine.

if aws dynamodb describe-table --table-name terraform-xand --region $REGION > /dev/null 2>&1 ; then
    info "dynamodb locking table already exists."
else
    info "Creating dynamodb locking table ..."
    aws dynamodb create-table \
        --table-name terraform-xand \
        --region $REGION \
        --attribute-definitions AttributeName=LockID,AttributeType=S \
        --key-schema AttributeName=LockID,KeyType=HASH \
        --billing-mode PROVISIONED \
        --provisioned-throughput=ReadCapacityUnits=5,WriteCapacityUnits=5
fi

#   These sed commands find the strings "bucket" and "region" with any number of spaces from that until the equals sign, and then replace the whole line with the configuration we want.

sed -i "/bucket[[:space:]]*=/c\    bucket         = \"$STORAGE_CONTAINER\"" main.tf
sed -i "/region[[:space:]]*=/c\    region         = \"$REGION\"" main.tf

info "Updated main.tf with bucket name and region name."

gcp_project = "{GCP_PROJECT}"
gcp_region = "{GCP_REGION}"
gcp_zone = "{GCP_ZONE}"

cluster_name = "{CLUSTER_NAME}"
cluster_username = ""
cluster_password = ""

gcp_machine_type   = "n1-standard-8"
gcp_min_node_count = 2
gcp_max_node_count = 5

resource "google_compute_router" "xand" {
  project = var.gcp_project
  name    = var.cluster_name
  region  = google_compute_subnetwork.xand.region
  network = google_compute_network.xand.self_link
}

resource "google_compute_address" "xand_nat" {
  project = var.gcp_project
  count  = 2
  name   = "xand-nat-manual-ip-${var.cluster_name}-${count.index}"
  region = google_compute_subnetwork.xand.region
}

resource "google_compute_router_nat" "xand" {
  project = var.gcp_project
  name                               = var.cluster_name
  router                             = google_compute_router.xand.name
  region                             = google_compute_router.xand.region

  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = google_compute_address.xand_nat.*.self_link

  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                    = google_compute_subnetwork.xand.self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

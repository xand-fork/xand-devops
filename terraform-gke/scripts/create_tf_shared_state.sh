#!/bin/bash

set -o errexit # abort on nonzero exit status
set -o nounset # abort on unbound variable
set -o pipefail # abort if any element of a pipeline fails

function helptext () {
    local text=$(cat <<EOF
    Set up GCP Storage Bucket for Terraform workspace

    Arguments
        STORAGE_CONTAINER (Required) = Google Storage Bucket name.
EOF
          )
    echo "$text"
}

function error () {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

if [[ $# -eq 0 ]]; then
    error "Not enough arguments provided"
fi

STORAGE_CONTAINER=${1:?"$(error 'STORAGE_CONTAINER must be set')"}

#   skip to templating change if bucket already exists

if gsutil ls -b gs://$STORAGE_CONTAINER > /dev/null 2>&1 ; then
    info "Bucket found in selected gcloud project"
else
    info "Creating bucket ..."
    gsutil mb gs://$STORAGE_CONTAINER
    gsutil versioning set on gs://$STORAGE_CONTAINER
    gsutil versioning get gs://$STORAGE_CONTAINER
fi

#   update main.tf with bucket name

info "Updating ../main.tf with bucket name ..."

sed -i "/bucket[[:space:]]*=/c\    bucket  = \"$STORAGE_CONTAINER\"" main.tf
info  "Terraform main.tf file updated with new bucket name ..."

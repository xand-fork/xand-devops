#!/usr/bin/env bash

set -o errexit   # abort on nonzero exitstatus
set -eo pipefail # fail fast with exit code, even on pipelined commands
set -o nounset   # abort on unbound variable
set -x # print commands

echo $ARTIFACTORY_USER

TRACING_UTILS_URL="https://transparentinc.jfrog.io/artifactory/artifacts-internal/tracing-utils/tracing-utils-0.0.4.zip"
curl --fail -u${ARTIFACTORY_USER}:${ARTIFACTORY_PASS} -Lo /tmp/tracing-utils.zip "$TRACING_UTILS_URL"
unzip -o /tmp/tracing-utils.zip -d /tmp/tracing-utils

/tmp/tracing-utils/install-debian-deps.sh

# Copy scripts from zip onto path
cp /tmp/tracing-utils/*.sh /usr/local/bin/

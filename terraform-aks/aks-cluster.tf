locals {
  // Replace anything not matching [a-z0-9] with "" and trim to 12 characters
  cluster_node_pool_name = substr(replace(var.cluster_name, "/[^a-z0-9]/", ""), 0, 12)
}

data "azurerm_kubernetes_service_versions" "current" {
  location = azurerm_resource_group.xand.location
  version_prefix = var.cluster_version
  include_preview = "false"
}

resource "azurerm_kubernetes_cluster" "xand" {
  name                = var.cluster_name
  location            = azurerm_resource_group.xand.location
  resource_group_name = azurerm_resource_group.xand.name
  dns_prefix          = var.cluster_name
  kubernetes_version  = data.azurerm_kubernetes_service_versions.current.latest_version

  linux_profile {
    admin_username    = "ubuntu"

    ssh_key {
      key_data        = file(var.ssh_public_key)
    }
  }

  default_node_pool {
    name                  = local.cluster_node_pool_name
    enable_auto_scaling   = true
    enable_node_public_ip = false
    min_count             = var.azure_node_min_count
    max_count             = var.azure_node_max_count
    vm_size               = var.azure_vm_size
    vnet_subnet_id        = azurerm_subnet.xand_private_subnet.id
  }
/**
Bug: "Once you create an AKS cluster, you cannot change the load balancer SKU for that cluster." (Basic vs Standard)
See: https://docs.microsoft.com/en-us/azure/aks/load-balancer-standard#use-the-standard-sku-load-balancer

NAT and External IP deployed are of type Standard.

The default LBs deployed as a result of K8s service of type LoadBalancer were Basic.

Cluster's VM Scale Set was entering an error state:`SubnetWithNatGatewayAndBasicSkuResourceNotAllowed`.

Azure cli deploys Standard LB config for clusters by default, but Terraform deploys Basic.

Specify here to ensure nginx-ingress and whitelister-nginx Ingresses get Standard LoadBalancers
*/
  network_profile {
    network_plugin = "azure"
    load_balancer_sku = "Standard"
  }

/**
Bug: "Standard LoadBalancers are required when using a NAT. When setting the `load_balancer_sku`
above, it changes the behavior of `windows_profile.admin_username` below.

When no mention of `windows_profile` is added, subsequent `terraform apply`s
require destroy and replacement of cluster with:
```
  - windows_profile {
    - admin_username = "azureuser" -> null # forces replacement
    }
```
*/

  windows_profile {
    # When not set, terraform is not aligned with default.
    # Explicitly set so t4 does not destroy and replace cluster on each `apply`
    admin_username = "azureuser"
    admin_password = local.service_principal_secret
  }

  service_principal {
    client_id       = local.service_principal_application_id
    client_secret   = local.service_principal_secret
  }

  tags = {
    Environment = var.namespace_name
  }
}

resource "null_resource" "update_kubeconfig" {
  depends_on = [
    azurerm_kubernetes_cluster.xand
  ]

  provisioner "local-exec" {
    command = "az aks get-credentials -n ${azurerm_kubernetes_cluster.xand.name} -g ${azurerm_kubernetes_cluster.xand.resource_group_name}"
  }
}

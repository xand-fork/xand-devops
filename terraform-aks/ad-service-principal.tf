# Create an application
resource "azuread_application" "k8s_ad_app" {
  name = var.ad_app_name

  count = local.service_principal_provided ? 0 : 1
}

# Create a service principal
resource "azuread_service_principal" "k8s_ad_service_principal" {
  application_id = azuread_application.k8s_ad_app[0].application_id

  count = local.service_principal_provided ? 0 : 1
}

# Reference use for Azure Service Principals in Terraform
# https://github.com/terraform-providers/terraform-provider-azuread/issues/40#issuecomment-477076926
# Generate random password to be used for Service Principal
resource "random_password" "service_principal_secret" {
  length  = 32
  special = true

  count = local.service_principal_provided ? 0 : 1
}

resource "azuread_service_principal_password" "k8s_ad_service_principal_secret" {
  service_principal_id = azuread_service_principal.k8s_ad_service_principal[0].id
  value = random_password.service_principal_secret[0].result
  end_date = "2099-01-01T01:02:03Z"

  count = local.service_principal_provided ? 0 : 1
}

data "azuread_service_principal" "service_principal" {
  application_id = var.service_principal_application_id

  count = local.service_principal_provided ? 1 : 0
}

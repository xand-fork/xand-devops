#
# Provider Configuration
#

provider "azurerm" {
  version = "=2.0.0"
  features {}
}

provider "azuread" {
  version = "=0.7.0"
}

provider "null" {
  version = "=2.1"
}

provider "random" {
  version = "=2.2"
}


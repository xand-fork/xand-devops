locals {
  service_principal_provided = "${ var.service_principal_application_id != null && var.service_principal_secret != null }"

  service_principal_application_id = (
    local.service_principal_provided
      ? var.service_principal_application_id
      : azuread_service_principal.k8s_ad_service_principal[0].application_id
  )
  service_principal_object_id = (
    local.service_principal_provided
      ? data.azuread_service_principal.service_principal[0].object_id
      : azuread_service_principal.k8s_ad_service_principal[0].object_id
  )
  service_principal_secret = (
    local.service_principal_provided
      ? var.service_principal_secret
      : azuread_service_principal_password.k8s_ad_service_principal_secret[0].value
  )
}
